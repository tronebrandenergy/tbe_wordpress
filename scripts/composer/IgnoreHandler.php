<?php
/**
 * @file
 * Contains TBE\composer\IgnoreHandler.
 */

namespace TBE\composer;

use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;

class IgnoreHandler {
  public static function addPackageToIgnore(Event $event) {
    $base_path = '/web/wp-content/plugins/';
    $fs = new Filesystem();
    $packages = $event->getComposer()->getRepositoryManager()->getLocalRepository()->getPackages();
    $file = file('.gitignore', FILE_IGNORE_NEW_LINES);

    foreach ($packages as $package) {
      // Update the gitignore file
      if(strpos($package->getName(), 'wpackagist') !== false) {
        if ($fs->exists('.gitignore')) {
          if (($tmp = strstr($package->getName(), '/')) !== false) {
            $name = $base_path . substr($tmp, 1);
            if (!in_array($name, $file)) {
              fwrite(fopen('.gitignore', 'a'), $name . "\n");
              $event->getIO()->write("Adding plugin " . $package->getName() . " to gitIgnore file");
            }
          }
        }
      }
    }
  }
}