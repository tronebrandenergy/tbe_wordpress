<?php

namespace TBE\composer;

class WPInstallConfigs {
  public static function addWPSettingsFile() {
    $systemConfig = 'tbesettings.example';
    $newConfig = 'web/wp-config.php';

    copy($systemConfig, $newConfig);
  }
}