<?php

/**
 * @file
 * Contains TBE\composer\GitHandler.
 */

namespace TBE\composer;

use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;

class GitHandler {

  public static function cleanVendorGitFolders(Event $event) {
    $fs = new Filesystem();
    $packages = $event->getComposer()->getRepositoryManager()->getLocalRepository()->getPackages();
    $installationManager = $event->getComposer()->getInstallationManager();

    foreach ($packages as $package) {
      if (preg_match('/^wpackagist-plugin\/.*/i', $package)) {
        continue;
      }

      // Get full path to package.
      $installPath = $installationManager->getInstallPath($package);

      // If .git folder exists, remove it.
      if ($fs->exists($installPath . '/.git')) {
        $fs->remove($installPath . '/.git');

        if ($fs->exists($installPath . '/.gitignore')) {
          $fs->remove($installPath . '/.gitignore');
        }

        // Get short folder name, from vendor down.
        $position = stripos($installPath, 'vendor/');
        $folder = $position ? substr($installPath, $position) : $installPath;
        print("Removed GIT folder from the ". $folder . "\n");
      }
    }
  }
  public static function cleanModuleGitFolders(Event $event) {
    $fs = new Filesystem();
    $packages = $event->getComposer()->getRepositoryManager()->getLocalRepository()->getPackages();
    $installationManager = $event->getComposer()->getInstallationManager();

    foreach ($packages as $package) {
      if (preg_match('/^wpackagist-plugin\/.*/i', $package)) {
        continue;
      }

      // Get full path to package.
      $installPath = $installationManager->getInstallPath($package);

      // If .git folder exists, remove it.
      if ($fs->exists($installPath . '/.git')) {
        $fs->remove($installPath . '/.git');

        if ($fs->exists($installPath . '/.gitignore')) {
          $fs->remove($installPath . '/.gitignore');
        }

        // Get short folder name, from vendor down.
        $position = stripos($installPath, 'web/wp-content/plugins/');
        $folder = $position ? substr($installPath, $position) : $installPath;
        print("Removed GIT folder from the ". $folder . "\n");
      }
    }
  }

}
